"""Main
"""

import logging
import os
import shutil
from tomplate.data import Dataset
from tomplate.environment import Environment

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def main():
    logger.info("Running in %s", os.getcwd())
    logger.info("Loading environment")
    env = Environment()
    logger.info("Loading dataset")
    dataset = Dataset.load_dir_yaml()
    if not os.path.exists("./dist"):
        os.mkdir("./dist")
    logger.info("Copying public files")
    shutil.copytree(env.public_dir, "./dist", dirs_exist_ok=True)
    for template in env.templates:
        target_path = os.path.abspath(os.path.join("./dist", template.relative_path))
        logger.info("Processing template file %s -> %s", template.relative_path, target_path)
        target_subdir = os.path.dirname(target_path)
        if not os.path.exists(target_subdir):
            os.makedirs(target_subdir)
        with open(target_path, "w", encoding="utf-8") as fid:
            fid.write(template.template.render(data=dataset))


if __name__ == "__main__":
    main()
