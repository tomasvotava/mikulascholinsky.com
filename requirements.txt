jinja2==3.1.4 ; python_version >= "3.8" and python_version < "4.0"
markupsafe==2.1.5 ; python_version >= "3.8" and python_version < "4.0"
pyyaml==5.4.1 ; python_version >= "3.8" and python_version < "4.0"
