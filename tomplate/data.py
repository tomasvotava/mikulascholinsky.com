"""Data loader
"""

import os
import logging
from glob import glob
from typing import Any, Dict, List, Optional, Union
import yaml

from tomplate.utils import root_join

logger = logging.getLogger(__name__)


class Dataset:
    """This class encapsulates a dataset found in ./data directory"""

    def __init__(self):
        self._data: Dict[str, Union[Dict[str, Any], List[Any]]] = {}

    def add(self, name: str, data: Union[Dict[str, Any], List[Any]]):
        """Add named data into the dataset"""
        self._data.update({name: data})

    @property
    def datasets(self) -> Dict[str, Union[Dict[str, Any], List[Any]]]:
        """Return all datasets"""
        return self._data

    @classmethod
    def load_dir_yaml(cls, base_path: Optional[str] = None) -> "Dataset":
        """Load dataset from *.yml files found in base_path"""
        base_path = base_path or root_join("data")
        matching = [*glob(os.path.join(base_path, "*.yml")), *glob(os.path.join(base_path, "*.yaml"))]
        dataset = cls()
        for fname in matching:
            logger.info("Loading data from dataset %s", fname)
            with open(fname, "r", encoding="utf-8") as fid:
                data = yaml.load(fid, Loader=yaml.Loader)
            dataset.add(os.path.splitext(os.path.basename(fname))[0], data)
        return dataset

    def __getattr__(self, name: str) -> Union[Dict[str, Any], List[Any]]:
        if name in self._data:
            return self._data[name]
        return []
