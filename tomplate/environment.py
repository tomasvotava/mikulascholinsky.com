"""Environment
"""

import os
from typing import Iterable, Optional
from jinja2 import Template as _Template

from tomplate.utils import root_join, ROOT_PATH, root_walk


class Template:
    """Template"""

    def __init__(self, fname: str):
        self.fname = fname
        with open(self.fname, "r", encoding="utf-8") as fid:
            self.template = _Template(fid.read())

    @property
    def relative_path(self) -> str:
        """Get template path relative to template root directory"""
        return os.path.relpath(self.fname, os.path.join(ROOT_PATH, "template"))


class PublicFile:
    """Public file"""

    def __init__(self, fname: str):
        self.fname = fname

    @property
    def relative_path(self) -> str:
        """Get public file relative path to public root directory"""
        return os.path.relpath(self.fname, os.path.join(ROOT_PATH, "public"))


class Environment:
    """Environment"""

    def __init__(self):
        self.template_dir = root_join("template")
        self.public_dir = root_join("public")

    @property
    def files(self) -> Iterable[str]:
        """Return all template files in template_dir"""
        yield from root_walk(self.template_dir)

    @property
    def templates(self) -> Iterable[Template]:
        """Return all templates as a loaded jinja2's Template"""
        yield from (Template(fname) for fname in self.files)

    def load_template(self, fname: str) -> Template:
        """Load template as jinja2's Template"""
        return Template(fname)

    @property
    def public_files(self) -> Iterable[PublicFile]:
        """Return all public files from ./public directory"""
        yield from (PublicFile(fname) for fname in root_walk(self.public_dir))
