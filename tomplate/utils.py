"""Utils
"""

import os
from typing import Iterable, List, Tuple

ROOT_PATH = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))


def root_join(*paths: str) -> str:
    """Join parts of path to root (directory where tomplate directory is)"""
    return os.path.join(ROOT_PATH, *paths)


def root_walk(path: str) -> Iterable[str]:
    """Walk path using os.walk and yield files only"""
    for dirpath, _, files in os.walk(path):
        for fname in files:
            yield os.path.join(dirpath, fname)
